Rails.application.routes.draw do
  
  
  resources :products
  resources :users

  root :to => "static_pages#home"
  post 'sessions/new', to: 'sessions#create'
  get '/products', to: 'products#index'
  get '/signup', to: 'users#new'
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  get '/about', to: 'static_pages#about'

  get '/find', to: 'static_pages#contact'


  resources :password_resets,     only: [:new, :create, :edit, :update]
  get 'password_resets/new', to:  'password_resets#new'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
