class UserMailer < ApplicationMailer

  before_action :valid_user, only: [:edit, :update]
  


  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def password_reset(user)
    user = User.first
    user.reset_token = User.all
    UserMailer.password_reset(user)
  end

 

end
