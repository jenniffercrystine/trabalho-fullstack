class User < ApplicationRecord
	attr_accessor :reset_token
	has_many :orders
	has_secure_password
	mount_uploader :photo, PhotoUploader
	validates :email, presence: true, uniqueness: true, length: { maximum: 50 }
	UserMailer.password_reset(self).deliver_now

	def create_reset_digest
		self.reset_token = User.new_token
    	update_columns(reset_digest:  FILL_IN, reset_sent_at: FILL_IN)
  	
	end

  # Sends password reset email.
	def send_password_reset_email
	UserMailer.password_reset(self).deliver_now
	end
	


	def password_reset_expired?
		reset_sent_at < 2.hours.ago
	end


end
