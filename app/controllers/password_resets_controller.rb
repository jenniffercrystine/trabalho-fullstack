class PasswordResetsController < ApplicationController
  before_action :check_expiration, only: [:edit, :update]
  before_action :get_user,   only: [:edit, :update]
  before_action :valid_user, only: [:edit, :update]


  def new
  end

  def create
    @user = User.find_by(email: params[:password_reset][:email])
    if @user
      @user.create_reset_digest
      @user.send_password_reset_email
      flash[:info] = "Email com instruções enviado"
      redirect_to root_url
    else
      flash.now[:danger] = "Email não encontrado"
      render 'new'
    end
  end

  def edit
  end

  def update
    if params[:user][:password].empty?                  # Case (3)
      @user.errors.add(:password, "Não pode estar em branco!")
      render 'edit'
    elsif @user.update_attributes(user_params)          # Case (4)
      log_in @user
      flash[:success] = "Senha atualizada!"
      redirect_to @user
    else
      render 'edit'                                     # Case (2)
    end
  end

  private

  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end

  def get_user
    @user = User.find_by(email: params[:email])
  end



  def valid_user
    unless (@user &&   @user.authenticated?(:reset, params[:id]))
      redirect_to root_url
    end
  end


  def check_expiration
    if @user.password_reset_expired?
      flash[:danger] = "Redefinição de senha expirou!"
      redirect_to new_password_reset_url
    end
  end

end

