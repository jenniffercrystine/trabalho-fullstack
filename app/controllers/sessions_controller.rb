class SessionsController < ApplicationController
  before_action :logged, only: [:new]
  before_action :not_logged, only: [:destroy]
  def new
  end

  def create
    
    user = User.find_by(email: params[:session][:email])
    if user && user.authenticate(params[:session][:password])
      log_in(user)
      flash[:success] = 'Bem vinde!'
      redirect_to user
    
    else
      flash[:danger] = 'Email ou senha errados'
      render 'new'
    end
  end


  def destroy
    log_out
    redirect_to root_path
  end

  def logged
    if logged_in?
      redirect_to current_user
    end
  end

  def not_logged
    if !logged_in?
    redirect_to login_path
    end
  end
end
