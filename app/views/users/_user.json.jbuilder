json.extract! user, :id, :name, :admin, :password_digest, :cpf, :address, :created_at, :updated_at
json.url user_url(user, format: :json)
