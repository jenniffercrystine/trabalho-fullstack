class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.boolean :admin
      t.string :password_digest
      t.string :cpf
      t.string :address

      t.timestamps
    end
  end
end
